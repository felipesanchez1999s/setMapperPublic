package com.epam.rd.autocode;


import com.epam.rd.autocode.domain.Employee;
import com.epam.rd.autocode.domain.FullName;
import com.epam.rd.autocode.domain.Position;


import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

public class SetMapperFactory {

    public SetMapper<Set<Employee>> employeesSetMapper() {

        return new SetMapper<Set<Employee>>() {

            private HashMap<BigInteger,Employee>fullyDefinedEmployee=new HashMap<>();

            @Override
            public Set<Employee> mapSet(ResultSet resultSet) {
                //HashMap<BigInteger,BigInteger>idToManager=new HashMap<>();
                //Set<Employee>employees=new HashSet<>();
                Set <Employee>finalEmployees=new HashSet<>();
                HashMap<BigInteger,Employee> myFullyDefinedEmployees=new HashMap<>();
                List<BigInteger> ids=new ArrayList<>();
                try {

                    while (resultSet.next()) {



                        ids.add(BigInteger.valueOf(resultSet.getInt("ID")));


                    }

                    for(BigInteger id:ids){
                        defineEmployee(id,resultSet,myFullyDefinedEmployees);
                    }


                    for(Employee e: myFullyDefinedEmployees.values()){
                        finalEmployees.add(e);
                    }


                }catch (SQLException sq){
                    sq.printStackTrace();
                }



                return finalEmployees;
            }


             void defineEmployee( BigInteger id,ResultSet resultSet,HashMap<BigInteger,Employee> fullyDefinedEmployees){
                try {
                    resultSet.first();
                    do{
                        if(id.equals(BigInteger.valueOf(resultSet.getInt("ID")))){
                            if(resultSet.getInt("MANAGER")==0){
                                Employee hojaEmployee=mapRow(resultSet,null);
                                fullyDefinedEmployees.put(id,hojaEmployee);
                            }else{
                                Employee nodoEmploye=mapRow(resultSet,null);
                                BigInteger managerId=BigInteger.valueOf(resultSet.getInt("MANAGER"));
                                defineEmployee(managerId,resultSet,fullyDefinedEmployees);
                                Employee finalEmployee=new Employee(
                                        nodoEmploye.getId(),
                                        nodoEmploye.getFullName(),
                                        nodoEmploye.getPosition(),
                                        nodoEmploye.getHired(),
                                        nodoEmploye.getSalary(),
                                        fullyDefinedEmployees.get(managerId)
                                );

                                fullyDefinedEmployees.put(nodoEmploye.getId(),finalEmployee);
                            }
                            break;
                        }

                    }while(resultSet.next());
                } catch (SQLException e) {
                    throw new RuntimeException(e);
                }
            }
            public Employee mapRow(ResultSet resultSet,Employee manager) {
                Employee employee=null;
                try{
                    FullName fullName= new FullName(resultSet.getString("FIRSTNAME"),resultSet.getString("LASTNAME"),resultSet.getString("MIDDLENAME"));
                    String strPosition=resultSet.getString("POSITION");
                    Position position=Position.valueOf(strPosition);
                    Date hdate=resultSet.getDate("HIREDATE");
                    LocalDate hiredDate=hdate.toLocalDate();
                    employee=new Employee(BigInteger.valueOf((long)resultSet.getInt("ID")),fullName,position,hiredDate, BigDecimal.valueOf(resultSet.getDouble("SALARY")),manager);
                }catch (SQLException e){
                    e.printStackTrace();
                }

                return employee;
            }
        };


    }
}
